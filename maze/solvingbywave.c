
#include <stdio.h>
#include <stdlib.h>
#include "solvingbywave.h"

int solve_by_wave(int m, int n, int **maze)
{
    static int firstEnter = 0;
    int i = 0;
    int j = 0;
    int count = 0;
    int test = 0;
    int start_cell = 0;
    static int x_start;
    static int y_start;
    static int y_finish;
    static int x_finish;


    if (!firstEnter)
    {
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
            {
                if (*(*(maze + i) + j) == 2)
                {
                    x_start = j;
                    y_start = i;
                }
                if (*(*(maze + i) + j) == 0)
                {
                    x_finish = j;
                    y_finish = i;
                }
                if (*(*(maze + i) + j) == -4)
                    *(*(maze + i) + j) = 1;
            }
        firstEnter++;
    }

        (*(*(maze + y_start) + x_start))++;
        start_cell = *(*(maze + y_start) + x_start);
        for(count = start_cell; count > 1; count--)
            for(i = 1; i < m - 1; i++)
                for(j = 1; j < n - 1; j++)
                    if (*(*(maze + i) + j) == count && change_value(i, j, m, n, maze))
                        test++;

        if (check_goal(y_finish, x_finish, maze))
            test = 0;

    if (!test)
    {
        draw_track(y_finish, x_finish, maze);

        for(i = 1; i < m - 1; i++)
            for(j = 1; j < n - 1; j++)
            {
                if (*(*(maze + i) + j) > 0)
               *(*(maze + i) + j) = 1;
                if (*(*(maze + i) + j) == -5)
               *(*(maze + i) + j) = -4;
            }

        *(*(maze + y_start) + x_start) = 2;
        *(*(maze + y_finish) + x_finish) = 0;
        firstEnter = 0;
        return 0;
    }
    return 1;
}

int change_value(int y, int x, int m, int n, int **maze)
{
    int vt = 0;
    int center_val;
    int test_change_val = 0;
    center_val = *(*(maze + y) + x);

/*up*/
            if (y > 0)
            {
                vt = *(*(maze + y - 1) + x);
                if (center_val - vt == 2)
                {
                    *(*(maze + y - 1) + x) = center_val - 1;
                    test_change_val++;
                }
            }
/*right*/
            if (x < n - 1)
            {
                vt = *(*(maze + y) + x + 1);
                if (center_val - vt == 2)
                {
                    *(*(maze + y) + x + 1) = center_val - 1;
                    test_change_val++;
                }
            }
/*down*/
            if (y < m - 1)
            {
                vt = *(*(maze + y + 1) + x);
                if (center_val  - vt == 2)
                {
                    *(*(maze + y + 1) + x) = center_val - 1;
                    test_change_val++;
                }
            }
/*left*/
            if (x > 0)
            {
                vt = *(*(maze + y) + x - 1);
                if (center_val - vt == 2)
                {
                    *(*(maze + y) + x - 1) = center_val - 1;
                    test_change_val++;
                }
            }

    if(test_change_val)
    {
        return 1;
    }
    else
        return 0;
}

int check_goal(int y, int x, int **maze)
{
    if (*(*(maze + y) + x) == 1)
        return 1;
    else
        return 0;
}


void draw_track(int y, int x, int **maze)
{
    int i = 0;
    int test_draw = 1;

        for(; test_draw;)
        {
            test_draw = 0;
            for(i = 0; i < 4; i++)
                switch(i)
                {
                    case 0: // up
                            if (*(*(maze + y - 1) + x) - *(*(maze + y) + x) == 1)
                            {
                                *(*(maze + y) + x) = -5;
                                test_draw++;
                                y--;
                            }
                        break;

                    case 1: // right
                            if (*(*(maze + y) + x + 1) - *(*(maze + y) + x) == 1)
                            {
                                *(*(maze + y) + x) = -5;
                                test_draw++;
                                x++;
                            }
                        break;

                    case 2: // down
                            if (*(*(maze + y + 1) + x) - *(*(maze + y) + x) == 1)
                            {
                                *(*(maze + y) + x) = -5;
                                test_draw++;
                                y++;
                            }
                        break;

                    case 3: // left
                            if (*(*(maze + y) + x - 1) - *(*(maze + y) + x) == 1)
                            {
                                *(*(maze + y) + x) = -5;
                                test_draw++;
                                x--;
                            }
                        break;
                }
        }
        *(*(maze + y) + x) = 0;
}
