#include <stdio.h>
#include <stdlib.h>
#include "mazecreation.h"

/// FIRST FIELD FILLING
void fill_maze(int m, int n, int **maze)
{
    int i = 0;
    int j = 0;

    for (i = 0; i < m; i++)
        for(j = 0; j < n; j++)
            if (i == 0 || j == 0 || i == (m - 1) || j == (n - 1))
                *(*(maze + i) + j) = -20; // border
            else
                *(*(maze + i) + j) = -10;
}

/// CREATION MAZE
void create_maze(int m, int n, int **maze)
{
    int i = 0;
    int j = 0;
    int x = 1 + rand()% (n - 2);
    int y = 1 + rand()% (m - 2);
    int direction = 0;

    do
    {
        direction = 0;
            direction = check_direction(y, x, maze);
            if (direction)
                move_cell(&y, &x, direction, maze);
    }
    while(direction);

    for(i = 1; i < m - 1; i++)
        for(j = 1; j < n - 1; j++)
            if (*(*(maze + i) + j) == 1 || *(*(maze + i) + j) == 7)
                *(*(maze + i) + j) = 1;
}

/// CHECK DIRECTION
int check_direction(int y, int x, int **maze)
{
    int test_arr[4] = {0};
    int direction = 0;
    int i = 0;
    int vt = 0;

    for(i = 0; i < 4; i++)
        switch(i)
        {
            case 0: /*up*/
                    vt = *(*(maze + y - 1) + x);
                    if (vt != -20 && vt != 7 && vt != 1 && check_neighbors(y - 1, x, 1, maze))
                        test_arr[i] = i + 1;
                    break;
            case 1: /*right*/
                    vt = *(*(maze + y) + x + 1);
                    if (vt != -20 && vt != 7 && vt != 1 && check_neighbors(y, x + 1, 2, maze))
                        test_arr[i] = i + 1;
                    break;
            case 2: /*down*/
                    vt = *(*(maze + y + 1) + x);
                    if (vt != -20 && vt != 7 && vt != 1 && check_neighbors(y + 1, x, 3, maze))
                        test_arr[i] = i + 1;
                    break;
            case 3: /*left*/
                    vt = *(*(maze + y) + x - 1);
                    if (vt != -20 && vt != 7 && vt != 1 && check_neighbors(y, x - 1, 4, maze))
                        test_arr[i] = i + 1;
                    break;
        }

    for (i = 0; i < 4; i++)
        if (test_arr[i])
        {
            direction = test_arr[i];
            break;
        }

    if(direction)
    {
        i = rand() % 4;
        do
        {
            i++;
            i = (i + 4) % 4;
            direction = test_arr[i];
        }
        while (test_arr[i] == 0);

        return direction;
    }
    else
    {
        for(i = 0; i < 4; i++)
            switch(i)
            {
                case 0: /*up*/
                        vt = *(*(maze + y - 1) + x);
                        if (vt == 1)
                        {
                            direction = 1;
                            *(*(maze + y) + x) = 7;
                            return direction;
                        }
                        break;
                case 1: /*right*/
                        vt = *(*(maze + y) + x + 1);
                        if (vt == 1)
                        {
                            direction = 2;
                            *(*(maze + y) + x) = 7;
                            return direction;
                        }
                        break;
                case 2: /*down*/
                        vt = *(*(maze + y + 1) + x);
                        if (vt == 1)
                        {
                            direction = 3;
                            *(*(maze + y) + x) = 7;
                            return direction;
                        }
                        break;
                case 3: /*left*/
                        vt = *(*(maze + y) + x - 1);
                        if (vt == 1)
                        {
                            direction = 4;
                            *(*(maze + y) + x) = 7;
                            return direction;
                        }
                        break;
            }
        *(*(maze + y) + x) = 7;
        return direction;
    }
}

/// CHECKING NEIGHBORS
int check_neighbors(int y, int x, int direction, int **maze)
{
    int i = 0;
    int j = 0;
    int vt = 0;
    int test = 1;
    int l = 0;
    int u = 0;
    int r = 0;
    int d = 0;

    switch (direction)
    {
    case    1: u = 1;
            break;
    case    2: r = 1;
            break;
    case    3: d = 1;
            break;
    case    4: l = 1;
            break;
    }

    for(i = -1 + d; i <= 1 - u; i++)
        for(j = -1 + r; j <= 1 - l; j++)
            switch(i)
            {
                case   -1:  switch(j)
                            {
                                case   -1:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;

                                case    0:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;

                                case    1:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;
                            }
                            break;
                case    0:  switch(j)
                            {
                                case   -1:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;

                                case    1:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;
                            default: break;
                            }
                            break;
                case    1:  switch(j)
                            {
                                case   -1:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;
                                case    0:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;
                                case    1:  vt = *(*(maze + y + i) + x + j);
                                            if  ( vt == 1 || vt == 7)
                                            {
                                                test = 0;
                                                return test;
                                            }
                                            break;
                            }
                            break;
            }
    return test;
}

/// MOVING CELL
void move_cell(int *y, int *x, int direction, int **maze)
{
    switch (direction)
    {
    case 1:
            (*y)--;
            *(*(maze + *y) + *x) = 1;
            break;
    case 2:
            (*x)++;
            *(*(maze + *y) + *x) = 1;
            break;
    case 3:
            (*y)++;
            *(*(maze + *y) + *x) = 1;
            break;
    case 4:
            (*x)--;
            *(*(maze + *y) + *x) = 1;
            break;
    }
}
