#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

#include "glfw3.h"
#include "glfw3native.h"
#include "glut.h"

#include "stb.h"
#include "stb_image.h"
#include "mazeoutput.h"
#include "menu.h"

static int MainImageWidth  [20];
static int MainImageHeight [20];
static int BpP             [20];
static GLuint MainTextName [20];
static stbi_uc *TextDataPtr[20];
static GLuint *MenuItemBar[MenuCap];
static MenuItemSct MenuItem[MenuCap];
int m;
int n;

void draw_menu(int *EnterPressed, int *ESCPressed, int *MenuDirection, MenuEnum *CurMenuPos, MenuEnum *PosForCheck, int *MenuFlag)
{

    static int menuinit        = 1;
    int i                      = 0;
    MenuEnum MenuItemStart;
    MenuEnum MenuItemFinish;
    MenuEnum MenuMoveStart;

    if (menuinit)
    {
        for (i = 0; i < 20; ++i)
        {
            MainImageWidth[i]   = 0;
            MainImageHeight [i] = 0;
            BpP [i]             = 0;
            MainTextName[i]     = 0;
        }

        char buff[50];
        for (i = 0; i < 20; ++i)
        {
            glGenTextures(1, &MainTextName[i]);
            if (i < 9)
                sprintf(buff, "texture/button/button0%i.jpg", i + 1);
            else
                sprintf(buff, "texture/button/button%i.jpg", i + 1);
            TextDataPtr[i] = stbi_load(buff, &MainImageWidth[i], &MainImageHeight[i], &BpP[i], 3);
            glGenTextures(1, &MainTextName[i]);
            glBindTexture (GL_TEXTURE_2D, MainTextName[i]);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glTexImage2D(GL_TEXTURE_2D, 0, 3, MainImageWidth[i], MainImageHeight[i], 0, GL_RGB, GL_UNSIGNED_BYTE, TextDataPtr[i]);
        }

        for (i = 0; (unsigned)i < MenuCap; ++i)
        {
            MenuItem[i].ActStat = 0;
        }

        MenuItemBar[MainPlayM]  = &MainTextName[0];
        MenuItemBar[MainQuitM]  = &MainTextName[1];
        MenuItemBar[MainInfoM]  = &MainTextName[2];

        MenuItemBar[MainAYS]    = &MainTextName[16];
        MenuItemBar[MainNoM]    = &MainTextName[17];
        MenuItemBar[MainYesM]   = &MainTextName[18];

        MenuItemBar[LevelM]     = &MainTextName[3];
        MenuItemBar[StyleM]     = &MainTextName[7];
        MenuItemBar[StartM]     = &MainTextName[11];

        MenuItemBar[LowM]       = &MainTextName[4];
        MenuItemBar[MediumM]    = &MainTextName[5];
        MenuItemBar[HardM]      = &MainTextName[6];

        MenuItemBar[DigitalM]   = &MainTextName[8];
        MenuItemBar[NatureM]    = &MainTextName[9];
        MenuItemBar[SnowM]      = &MainTextName[10];

        MenuItemBar[GameHelpM]  = &MainTextName[12];
        MenuItemBar[GamePromptM]= &MainTextName[13];
        MenuItemBar[GameWaveM]  = &MainTextName[14];
        MenuItemBar[GameQuitM]  = &MainTextName[15];

        MenuItemBar[GameAYS]    = &MainTextName[16];
        MenuItemBar[GameNoM]    = &MainTextName[17];
        MenuItemBar[GameYesM]   = &MainTextName[18];

        MenuItemBar[NewGameM]   = &MainTextName[19];
        MenuItemBar[GameFQuitM] = &MainTextName[1];

        MenuItemBar[GFinAYS]    = &MainTextName[16];
        MenuItemBar[GFinNo]     = &MainTextName[17];
        MenuItemBar[GFinYes]    = &MainTextName[18];

        *CurMenuPos = MainPlayM;
        MenuItem[*CurMenuPos].ActStat = 1;
    menuinit = 0;
    }

    switch(*CurMenuPos)
    {
        case MainPlayM: /*MAIN MENU*/
            if (*EnterPressed)
            {
                *CurMenuPos = LevelM;
                (*MenuFlag)++;
                break;
            }
        case MainQuitM:
            if (*EnterPressed)
            {
                *CurMenuPos = MainNoM;
                (*MenuFlag)++;
                break;
            }
        case MainInfoM:
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                (*MenuFlag) = 0;
                break;
            }
            MenuMoveStart = MainPlayM;
            MenuItemStart = MainPlayM;
            MenuItemFinish = MainInfoM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;

        case MainAYS: /*MAIN QUIT*/
        case MainNoM:
            if (*EnterPressed)
            {
                *CurMenuPos = MainPlayM;
                (*MenuFlag)--;
                break;
            }
        case MainYesM:
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                glDeleteTextures(20, MainTextName);
                *MenuFlag = 0;
                break;
            }
            if (*ESCPressed)
            {
                (*MenuFlag)--;
                *CurMenuPos = MainPlayM;
                break;
            }
            MenuMoveStart = MainNoM;
            MenuItemStart = MainAYS;
            MenuItemFinish = MainYesM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;

        case LevelM: /*OPTIONS MENU*/
            if (*EnterPressed)
            {
                *CurMenuPos = LowM;
                (*MenuFlag)++;
                break;
            }
        case StyleM:
            if (*EnterPressed)
            {
                *CurMenuPos = DigitalM;
                (*MenuFlag)++;
                break;
            }
        case StartM:
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = GameHelpM;
                *MenuFlag = 0;
                break;
            }
            if (*ESCPressed)
            {
                (*MenuFlag)--;
                *CurMenuPos = MainPlayM;
                break;
            }
            MenuMoveStart = LevelM;
            MenuItemStart = LevelM;
            MenuItemFinish = StartM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;

        case LowM: /*LEVEL MENU*/
            if (*EnterPressed)
            {
                (*MenuFlag)--;
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = LevelM;
                break;
            }
        case MediumM:
            if (*EnterPressed)
            {
                (*MenuFlag)--;
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = LevelM;
                break;
            }
        case HardM:
            if (*EnterPressed)
            {
                (*MenuFlag)--;
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = LevelM;
                break;
            }
            if (*ESCPressed)
            {
                (*MenuFlag)--;
                *CurMenuPos = LevelM;
                break;
            }
            MenuMoveStart = LowM;
            MenuItemStart = LowM;
            MenuItemFinish = HardM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;

        case DigitalM: /*STYLE MENU*/
            if (*EnterPressed)
            {
                (*MenuFlag)--;
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = StyleM;
                break;
            }
        case NatureM:
            if (*EnterPressed)
            {
                (*MenuFlag)--;
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = StyleM;
                break;
            }
        case SnowM:
            if (*EnterPressed)
            {
                (*MenuFlag)--;
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = StyleM;
                break;
            }
            if (*ESCPressed)
            {
                (*MenuFlag)--;
                *CurMenuPos = StyleM;
                break;
            }
            MenuMoveStart = DigitalM;
            MenuItemStart = DigitalM;
            MenuItemFinish = SnowM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;

        case GameHelpM: /*GAME MENU*/
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                *MenuFlag = 0;
                break;
            }
        case GamePromptM:
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                *MenuFlag = 0;
                break;
            }
        case GameWaveM:
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                *MenuFlag = 0;
                break;
            }
        case GameQuitM:
            if (*EnterPressed)
            {
                *CurMenuPos = GameAYS;
                (*MenuFlag)++;
                break;
            }
            if (*ESCPressed)
            {
                (*MenuFlag)--;
                break;
            }
            MenuMoveStart = GameHelpM;
            MenuItemStart = GameHelpM;
            MenuItemFinish = GameQuitM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;

        case GameAYS:/*QUIT GAME CURENT*/
        case GameNoM:
            if (*EnterPressed)
            {
                (*MenuFlag)--;
                *CurMenuPos = GameHelpM;
                break;
            }
        case GameYesM:
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = NewGameM;
                glDeleteTextures(20, MainTextName);
                menuinit = 1;
                *MenuFlag = 0;
                break;
            }
            if (*ESCPressed)
            {
                (*MenuFlag)--;
                *CurMenuPos = GameHelpM;
                break;
            }
            MenuMoveStart = GameNoM;
            MenuItemStart = GameAYS;
            MenuItemFinish = GameYesM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);

        case NewGameM: /*FINISH MENU*/
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;
                *CurMenuPos = MainPlayM;
                for (i = 0; (unsigned)i < MenuCap; ++i)
                {
                    MenuItem[i].ActStat = 0;
                }
                break;
            }
        case GameFQuitM:
            if (*EnterPressed)
            {
                *CurMenuPos = GFinNo;
                (*MenuFlag)++;
                break;
            }
            MenuMoveStart = NewGameM;
            MenuItemStart = NewGameM;
            MenuItemFinish = GameFQuitM;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;

        case GFinAYS: /*TOTAL EXIT MENU*/
        case GFinNo:
            if (*EnterPressed)
            {
                *CurMenuPos = NewGameM;
                (*MenuFlag)--;
                break;
            }
        case GFinYes:
            if (*EnterPressed)
            {
                *PosForCheck = *CurMenuPos;;
                glDeleteTextures(20, MainTextName);
                *MenuFlag = 0;
                break;
            }
            if (*ESCPressed)
            {
                (*MenuFlag)--;
                *CurMenuPos = NewGameM;
                break;
            }
            MenuMoveStart = GFinNo;
            MenuItemStart = GFinAYS;
            MenuItemFinish = GFinYes;
            draw_menu_items(CurMenuPos, MenuItemStart, MenuItemFinish);
            change_active_item(MenuDirection, CurMenuPos, MenuMoveStart, MenuItemFinish);
            break;
    default: break;
    }
*ESCPressed = 0;
*EnterPressed = 0;
}

void change_active_item(int *MenuDirection, MenuEnum *CurMenuPos, MenuEnum MenuMoveStart, MenuEnum MenuItemFinish)
{
    if (MenuItem[*CurMenuPos].ActStat == 0)
        MenuItem[*CurMenuPos].ActStat = 1;

    if (((int)*CurMenuPos + *MenuDirection) < (int)MenuMoveStart)
    {
        MenuItem[*CurMenuPos].ActStat = 0;
        *CurMenuPos = MenuItemFinish;
        MenuItem[*CurMenuPos].ActStat = 1;
        *MenuDirection = 0;
    }
    else    if (((int)*CurMenuPos + *MenuDirection) > (int)MenuItemFinish)
            {
                MenuItem[*CurMenuPos].ActStat = 0;
                *CurMenuPos = MenuMoveStart;
                MenuItem[*CurMenuPos].ActStat = 1;
                *MenuDirection = 0;
            }
            else
            {
                MenuItem[*CurMenuPos].ActStat = 0;
                *CurMenuPos += *MenuDirection;
                MenuItem[*CurMenuPos].ActStat = 1;
                *MenuDirection = 0;
            }
}

void draw_menu_items(MenuEnum *CurMenuPos, MenuEnum Start, MenuEnum Finish)
{
    unsigned int bA = 0;
    unsigned int i  = 0;
    unsigned int Am = 0;
    int dir         = 1;
    static int j    = 1;
    static int a[3] = {-2, 0, 2};

    Am = Finish - Start;
    for (i = 0, bA = Start; i < Am, bA <= Finish; ++i, ++bA)
    {
        glEnable(GL_TEXTURE_2D);
        glBindTexture (GL_TEXTURE_2D, *MenuItemBar[bA]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

        if (MenuItem[bA].ActStat == 0)
        {
            glColor3ub(0, 0, 0);
            glBegin(GL_QUADS);
               glTexCoord2d(0,0); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2, 30 + i * (BUTTON_HEIGHT + 5));
               glTexCoord2d(0,1); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2, 30 + BUTTON_HEIGHT + i * (BUTTON_HEIGHT + 5));
               glTexCoord2d(1,1); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2 + BUTTON_WIDTH, 30 + BUTTON_HEIGHT + i * (BUTTON_HEIGHT + 5));
               glTexCoord2d(1,0); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2 + BUTTON_WIDTH, 30 + i * (BUTTON_HEIGHT + 5));
            glEnd();
        }
        else
        {
            *CurMenuPos = bA;
            glColor3ub(0, 0, 0);
            glBegin(GL_QUADS);
               glTexCoord2d(0,0); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2 + a[j] - 3, 30 + i * (BUTTON_HEIGHT + 5) - 3);
               glTexCoord2d(0,1); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2 + a[j] - 3, 30 + BUTTON_HEIGHT + i * (BUTTON_HEIGHT + 5) + 3);
               glTexCoord2d(1,1); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2 + BUTTON_WIDTH + a[j] + 3, 30 + BUTTON_HEIGHT + i * (BUTTON_HEIGHT + 5) + 3);
               glTexCoord2d(1,0); glVertex2i(((n * 10) - BUTTON_WIDTH) / 2 + BUTTON_WIDTH + a[j] + 3, 30 + i * (BUTTON_HEIGHT + 5) - 3);
            glEnd();
            if (j == 2)
                dir = 0;
            if (j == 0)
                dir = 1;
            if (dir == 1)
                j++;
            else
                j--;
        }
    }
}
