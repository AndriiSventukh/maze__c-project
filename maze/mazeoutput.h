#ifndef MAZE_OUTPUT_H
#define MAZE_OUTPUT_H

#define SCREEN_WIDTH        1350
#define SCREEN_HEIGHT       690
#define BUTTON_WIDTH        258
#define BUTTON_HEIGHT       79

typedef unsigned char stbi_uc;

typedef enum
{
     Start
    ,Game
    ,Finish
}Stage;

typedef enum
{
     MainPlayM
    ,MainQuitM
    ,MainInfoM

    ,MainAYS
    ,MainNoM
    ,MainYesM

    ,LevelM
    ,StyleM
    ,StartM

    ,LowM
    ,MediumM
    ,HardM

    ,DigitalM
    ,NatureM
    ,SnowM

    ,GameHelpM
    ,GamePromptM
    ,GameWaveM
    ,GameQuitM

    ,GameAYS
    ,GameNoM
    ,GameYesM

    ,NewGameM
    ,GameFQuitM

    ,GFinAYS
    ,GFinNo
    ,GFinYes
,MenuCap
}MenuEnum;

typedef enum
{
    up      = 1
   ,right   = 2
   ,down    = 3
   ,left    = 4
}dEnum;

typedef enum
{
    lowLevel = 1
   ,mediumLevel
   ,hardLevel
} lenum;

typedef enum
{
    DigitalTexM = 1
   ,NatureTexM
   ,SnowTexM
}sEnum;

void maze_engine_Ogl();
void check_param(MenuEnum*);
void key_click_clb (GLFWwindow*, int, int, int, int);
void resize_clb (GLFWwindow*, int, int);
void draw_start_image (int*, int*, GLuint*, stbi_uc*, int*);
void draw_info_image  (int*, int*, GLuint*, stbi_uc*, int*);
void draw_style_image (int*, int*, GLuint*, stbi_uc*, int*);
void draw_finish_image(int*, int*, GLuint*, stbi_uc*, int*);
void draw_wall(int *, int*, int*, int*, GLuint*, int, int, int**);
void move_character(int*, int*, int*, int*, int**);
int** create_apr_maze (int *, int*, int*, int*, int*, int*);

#endif // MAZE_OUTPUT_H
