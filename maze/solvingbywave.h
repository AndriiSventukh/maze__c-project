#ifndef WAVE_SOLVING_H
#define WAVE_SOLVING_H

int solve_by_wave(int, int, int**);
int check_goal(int, int, int**);
int change_value(int, int, int, int, int**);
void draw_track(int, int, int**);

#endif // WAVE_SOLVING_H
