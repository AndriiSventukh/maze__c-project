#ifndef MENU_H
#define MENU_H

typedef struct
{
    int ActStat;
    GLuint *Text;
} MenuItemSct;

void draw_menu(int*, int*, int*, MenuEnum*, MenuEnum*, int*);
void draw_menu_items(MenuEnum*, MenuEnum, MenuEnum);
void change_active_item(int*, MenuEnum*, MenuEnum, MenuEnum);

#endif // MENU_H
