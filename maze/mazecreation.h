#ifndef MAZE_CREATION_H
#define MAZE_CREATION_H

void fill_maze(int, int, int **);
void create_maze(int, int, int **);
int check_direction (int, int, int **);
void move_cell(int *, int *, int, int **);
int check_neighbors(int, int, int, int **);

#endif // MAZE_CREATION_H
