#include <stdio.h>
#include <stdlib.h>
#include "solvingbyfullexpl.h"

/// SOLVING MAZE
void solve_maze(int m, int n, int **maze)
{
    int i = 0;
    int j = 0;
    int x_start;
    int y_start;
    int y_finish;
    int x_finish;
    int x;
    int y;
    int direction = 0;

    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
        {
            if (*(*(maze + i) + j) == 2)
            {
                x_start = j;
                y_start = i;
            }

            if (*(*(maze + i) + j) == -4)
                *(*(maze + i) + j) = 1;

            if (*(*(maze + i) + j) == 0)
            {
                x_finish = j;
                y_finish = i;
            }
        }
    x = x_start;
    y = y_start;

    *(*(maze + y_start) + x_start) = 4;
    *(*(maze + y_finish) + x_finish) = 1;\

    do
    {
        direction = 0;
            direction = check_right_direction(y, x, maze);
            if (direction)
                move_prisoner(&y, &x, direction, maze);
            if (x == x_finish && y == y_finish)
                break;
    }
    while(direction);

    draw_prompt(y_start, x_start, m, maze);

    for(i = 1; i < m - 1; i++)
        for(j = 1; j < n - 1; j++)
            if (*(*(maze + i) + j) == 5 || *(*(maze + i) + j) == 4)
                *(*(maze + i) + j) = 1;


    *(*(maze + y_start) + x_start) = 2;
    *(*(maze + y_finish) + x_finish) = 0;
    }

/// CHECK DIRECTION
int check_right_direction(int y, int x, int **maze)
{
    int test_arr[4] = {0};
    int direction = 0;

    int i = 0;
    int vt = 0;

    // checking     possible way
    for(i = 0; i < 4; i++)
        switch(i)
        {
            case 0: /*up*/
                    vt = *(*(maze + y - 1) + x);
                    if (vt == 1)
                        test_arr[i] = i + 1;
                    break;
            case 1: /*right*/
                    vt = *(*(maze + y) + x + 1);
                    if (vt == 1)
                        test_arr[i] = i + 1;
                    break;
            case 2: /*down*/
                    vt = *(*(maze + y + 1) + x);
                    if (vt == 1)
                        test_arr[i] = i + 1;
                    break;
            case 3: /*left*/
                    vt = *(*(maze + y) + x - 1);
                    if (vt == 1)
                        test_arr[i] = i + 1;
                    break;
        }

    for (i = 0; i < 4; i++)
        if (test_arr[i])
        {
            direction = test_arr[i];
            break;
        }

    if(direction)
    {
        i = rand() % 4;
        do
        {
            i++;
            i = (i + 4) % 4;
            direction = test_arr[i];
        }
        while (test_arr[i] == 0);

        return direction;
    }
    else
    {
        for(i = 0; i < 4; i++)
            switch(i)
            {
                case 0: /*up*/
                        vt = *(*(maze + y - 1) + x);
                        if (vt == 4)
                        {
                            direction = 1;
                            *(*(maze + y) + x) = 5;
                            return direction;
                        }
                        break;
                case 1: /*right*/
                        vt = *(*(maze + y) + x + 1);
                        if (vt == 4)
                        {
                            direction = 2;
                            *(*(maze + y) + x) = 5;
                            return direction;
                        }
                        break;
                case 2: /*down*/
                        vt = *(*(maze + y + 1) + x);
                        if (vt == 4)
                        {
                            direction = 3;
                            *(*(maze + y) + x) = 5;
                            return direction;
                        }
                        break;
                case 3: /*left*/
                        vt = *(*(maze + y) + x - 1);
                        if (vt == 4)
                        {
                            direction = 4;
                            *(*(maze + y) + x) = 5;
                            return direction;
                        }
                        break;
            }
        return direction;
    }
}

/// MOVING CELL
void move_prisoner(int *y, int *x, int direction, int **maze)
{

    switch (direction)
    {
    case 1:
            (*y)--;
            *(*(maze + *y) + *x) = 4;
            break;
    case 2:
            (*x)++;
            *(*(maze + *y) + *x) = 4;
            break;
    case 3:
            (*y)++;
            *(*(maze + *y) + *x) = 4;
            break;
    case 4:
            (*x)--;
            *(*(maze + *y) + *x) = 4;
            break;
    }
}

void draw_prompt(int y, int x, int stepAm, int **maze)
{
    int i = 0;
    int test_draw = 1;

        for(; stepAm && test_draw; --stepAm)
        {
            test_draw = 0;
            for(i = 0; i < 4; i++)
                switch(i)
                {
                    case 0: // up
                            if (*(*(maze + y - 1) + x) == 4)
                            {
                                *(*(maze + y) + x) = -4;
                                test_draw++;
                                y--;
                            }
                        break;

                    case 1: // right
                            if (*(*(maze + y) + x + 1) == 4)
                            {
                                *(*(maze + y) + x) = -4;
                                test_draw++;
                                x++;
                            }
                        break;

                    case 2: // down
                            if (*(*(maze + y + 1) + x) == 4)
                            {
                                *(*(maze + y) + x) = -4;
                                test_draw++;
                                y++;
                            }
                        break;

                    case 3: // left
                            if (*(*(maze + y) + x - 1) == 4)
                            {
                                *(*(maze + y) + x) = -4;
                                test_draw++;
                                x--;
                            }
                        break;
                }
        }
        *(*(maze + y) + x) = -4;
}
