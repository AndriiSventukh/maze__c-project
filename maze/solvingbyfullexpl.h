#ifndef MAZE_SOLVING_H
#define MAZE_SOLVING_H

void solve_maze(int, int, int **);
int check_right_direction(int, int, int **);
void move_prisoner(int*, int*, int, int **);
void draw_prompt(int, int, int, int **);

#endif // MAZE_SOLVING_H
