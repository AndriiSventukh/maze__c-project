#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

#include "glfw3.h"
#include "glfw3native.h"
#include "glut.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb.h"
#include "stb_image.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

#include "mazeoutput.h"
#include "mazecreation.h"
#include "solvingbywave.h"
#include "solvingbyfullexpl.h"
#include "menu.h"

lenum OrderLevel;
dEnum dir;
sEnum OrderStyle;
MenuEnum CurMenuPos;
MenuEnum PosForChek;
Stage GameStage;

int moveTest;
int MenuDirection;
int Info;
int initStyle;
int OrderCreateMaze;
int waveTest;
int winTest;
int promptTest;
int cheatTest;
int wNotSolved;
int finishTest;
int finishTestSw;
int MenuFlag;
int QuitGameFlag;
int EnterPressed;
int ESCPressed;
static int TimeDel;

extern int n;
extern int m;

//------------------------------------------------------------------------------------------------------------------------------
void maze_engine_Ogl()
{
    int **maze = NULL;
    n = 135;
    m = 69;
    int x_s         = 0;
    int y_s         = 0;
    int x_f         = 0;
    int y_f         = 0;

    GameStage       = Start;
    CurMenuPos      = MainPlayM;
    PosForChek      = MainPlayM;
    OrderLevel      = lowLevel;
    OrderStyle      = NatureTexM;
    MenuFlag        = 1;
    QuitGameFlag    = 0;
    OrderCreateMaze = 1;
    finishTest      = 0;
    wNotSolved      = 1;
    winTest         = 0;
    promptTest      = 0;
    cheatTest       = 0;
    waveTest        = 0;
    finishTestSw    = 0;
    initStyle       = 1;
    EnterPressed    = 0;
    MenuDirection   = 0;
    Info            = 0;
    TimeDel       = 150;

    GLFWwindow* window;
    if (!glfwInit())
        return;
    window = glfwCreateWindow(n * 10, m * 10, "MAZE", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return;
    }
    glfwSetWindowPos(window,8,30);
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_click_clb);
    glfwSetWindowSizeCallback (window, resize_clb);

    glViewport(0, 0, (n * 10), (m * 10));
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, (GLsizei)(n * 10), (GLsizei)(m * 10), 0, 0, 1);
    glfwSwapInterval(1);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_TEXTURE_2D);

    int MainImageWidth  [5];
    int MainImageHeight [5];
    int BpP             [5];
    GLuint MainTextName [5];
    stbi_uc *TextDataPtr[5] = {NULL};

    while (!glfwWindowShouldClose(window))
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glfwSetWindowSize(window, n*10, m*10);
        glViewport(0, 0, (n * 10), (m * 10));
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, (GLsizei)(n * 10), (GLsizei)(m * 10), 0, 0, 1);

        if (PosForChek != MainPlayM)
        {
            check_param(&PosForChek);
        }
        if (moveTest)
        {
           move_character(&x_s, &y_s, &x_f, &y_f, maze);
           moveTest = 0;
        }

        switch (GameStage)
        {
        case Start:
            draw_start_image(&MainImageWidth[0], &MainImageHeight[0], &MainTextName[0], TextDataPtr[0], &BpP[0]);
            if(MenuFlag)
            {
               draw_menu(&EnterPressed, &ESCPressed, &MenuDirection, &CurMenuPos, &PosForChek, &MenuFlag);
            }
            if(Info)
            {
               draw_info_image(&MainImageWidth[4], &MainImageHeight[4], &MainTextName[4], TextDataPtr[4], &BpP[4]);
            }
            break;

        case Game:
            if (OrderCreateMaze)
            {
                maze = create_apr_maze(&x_s, &y_s, &x_f, &y_f, &m, &n);
                OrderCreateMaze = 0;
            }
            draw_style_image(&MainImageWidth[3], &MainImageHeight[3], &MainTextName[3], TextDataPtr[3], &BpP[3]);
            if (promptTest && !waveTest)
            {
                solve_maze(m, n , maze);
            }
            if  (waveTest && wNotSolved && !winTest &&!MenuFlag)
            {
                wNotSolved = solve_by_wave(m, n , maze);
            }
            if (finishTestSw < 50 && (wNotSolved || TimeDel > 100))
            {
                draw_wall(&x_s, &y_s, &x_f, &y_f, &MainTextName[1], m, n, maze);
            }
            if(!Info && MenuFlag)
            {
               draw_menu(&EnterPressed, &ESCPressed, &MenuDirection, &CurMenuPos, &PosForChek, &MenuFlag);
            }
            if (finishTestSw >= 50 || !wNotSolved)
            {
                TimeDel--;
            }
            if (!TimeDel)
            {
                n = 135;
                m = 69;
                CurMenuPos = NewGameM;
                GameStage = Finish;
                MenuFlag++;
                wNotSolved = 1;
                finishTestSw = 0;
            }
            if(Info)
            {
               draw_info_image(&MainImageWidth[4], &MainImageHeight[4], &MainTextName[4], TextDataPtr[4], &BpP[4]);
            }
            break;

        case Finish:
            draw_finish_image(&MainImageWidth[2], &MainImageHeight[2], &MainTextName[2], TextDataPtr[2], &BpP[2]);
            if(MenuFlag)
            {
               draw_menu(&EnterPressed, &ESCPressed, &MenuDirection, &CurMenuPos, &PosForChek, &MenuFlag);
            }
            break;

        default: break;
        }

        Sleep(20);
        glfwSwapBuffers(window);
        glfwPollEvents();

        if (QuitGameFlag)
        {
            glfwSetWindowShouldClose(window, GL_TRUE);
        }
    }

    glfwTerminate();
    glDeleteTextures(5, MainTextName);
    if (maze != NULL)
    {
        m = _msize(maze)/sizeof(int**);
        for (int i = 0; i < m; ++i)
            free(*(maze + i));
        free(maze);
    }
    return;
}
//------------------------------------------------------------------------------------------------------------------------------

void resize_clb (GLFWwindow *apWindow, int aWidth, int aHeight)
{
    if (aWidth != n * 10 || aHeight != m *10)
    {
        glfwSetWindowSize(apWindow, n*10, m*10);
        glViewport(0, 0, (n * 10), (m * 10));
    }
}

//------------------------------------------------------------------------------------------------------------------------------
void check_param(MenuEnum *Menu)
{
    switch(*Menu)
    {
        case MainInfoM:
            Info++;
            break;

        case MainAYS:
            break;
        case MainNoM:
            break;
        case MainYesM:
            QuitGameFlag++;
            break;

        case LowM:
            OrderLevel = lowLevel;
            break;
        case MediumM:
            OrderLevel = mediumLevel;
            break;
        case HardM:
            OrderLevel = hardLevel;
            break;

        case DigitalM:
            initStyle++;
            OrderStyle = DigitalTexM;
            break;
        case NatureM:
            initStyle++;
            OrderStyle = NatureTexM;
            break;
        case SnowM:
            initStyle++;
            OrderStyle = SnowTexM;
            break;

        case StartM:
            OrderCreateMaze++;
            GameStage = Game;
            break;

        case GameHelpM:
            Info++;
            break;
        case GamePromptM:
            promptTest++;
            break;
        case GameWaveM:
            waveTest++;
            break;

        case GameYesM:
            QuitGameFlag++;
            break;

        case NewGameM:
            GameStage       = Start;
            CurMenuPos      = MainPlayM;
            PosForChek      = MainPlayM;
            OrderLevel      = lowLevel;
            OrderStyle      = NatureTexM;
            MenuFlag        = 1;
            QuitGameFlag    = 0;
            OrderCreateMaze = 1;
            finishTest      = 0;
            wNotSolved      = 1;
            winTest         = 0;
            promptTest      = 0;
            cheatTest       = 0;
            waveTest        = 0;
            finishTestSw    = 0;
            initStyle       = 1;
            EnterPressed    = 0;
            MenuDirection   = 0;
            Info            = 0;
            TimeDel       = 150;
            m = 69;
            n = 135;
            break;

        case GFinYes:
            QuitGameFlag++;
            break;

    default: break;
    }
    *Menu = MainPlayM;
}

//------------------------------------------------------------------------------------------------------------------------------
void key_click_clb(GLFWwindow *pWindow, int aKey, int aScanCode, int aAction, int aMods)
{        
    if(aAction == GLFW_PRESS || aAction == GLFW_REPEAT)
       {
           switch(aKey)
           {
               case GLFW_KEY_RIGHT:
               {
                   if (!MenuFlag)
                   {
                       dir = right;
                       moveTest++;
                   }
                   break;
               }
               case GLFW_KEY_LEFT:
               {
                   if (!MenuFlag)
                   {
                       dir = left;
                       moveTest++;
                   }
                   break;
               }
               case GLFW_KEY_DOWN:
               {
                   if (!MenuFlag)
                   {
                       dir = down;
                       moveTest++;
                       break;
                   }
                   else
                   {
                       MenuDirection++;
                       break;
                   }
               }
               case GLFW_KEY_UP:
               {
                   if (!MenuFlag)
                   {
                       dir = up;
                       moveTest++;
                       break;
                   }
                   else
                   {
                       MenuDirection--;
                       break;
                   }
               }
               case GLFW_KEY_C:
               {
                   if (!MenuFlag)
                   {
                       cheatTest++;
                       break;
                   }
               }
               case  GLFW_KEY_ENTER:
               {
                   EnterPressed++;
                   break;
               }
               case  GLFW_KEY_ESCAPE:
               {
                   if (Info)
                   {
                       Info = 0;
                       MenuFlag = 1;
                   }
                   else
                   {
                       if (MenuFlag)
                       {
                           ESCPressed++;
                       }
                       else
                           MenuFlag = 1;
                   }
                   break;
               }
           }
       }
}

//------------------------------------------------------------------------------------------------------------------------------
int **create_apr_maze(int *x_s, int *y_s, int *x_f, int *y_f, int *m, int *n)
{
    time_t t;
    srand((unsigned) time(&t));

    int i = 0;
    int **maze;

    switch (OrderLevel)
    {
    case lowLevel:
        *n = 70;
        *m = 37;
        break;
    case mediumLevel:
        *n = 90;
        *m = 46;
        break;
    case hardLevel:
        *n = 135;
        *m = 69;
        break;
    }

    maze = (int**)malloc((*m) * sizeof(int*));
    for (i = 0; i < *m; i++)
         *(maze + i) = (int*)calloc((*n), sizeof(int));

        fill_maze(*m, *n, maze);
        create_maze(*m, *n, maze);

        do // CREATING POINT OF START
        {
            *y_s = 1 + rand()% ((*m) - 2);
            *x_s = 1 + rand()% ((*n) / 3);
        }
        while (*(*(maze + *y_s) + *x_s) != 1);
        *(*(maze + *y_s) + *x_s) = 2;

        do // CREATING POINT OF FINISH
        {
            *y_f = (*m) - 1 - (1 + rand()% ((*m) - 2));
            *x_f = (*n) - 1 - (1 + rand()% ((*n) / 3));
        }
        while (*(*(maze + *y_f) + *x_f) != 1);

        *(*(maze + *y_f) + *x_f) = 0;

        return maze;
}

void draw_start_image(int *MainImWidth, int *MainImHeight, GLuint *MainTxtName, stbi_uc *TextDataPtr, int *BpPix)
{
    static int InitDrawStIm = 1;
    if (InitDrawStIm)
    {
        glGenTextures(1, MainTxtName);
        TextDataPtr = stbi_load("Texture/Intro/texture1.jpg", MainImWidth, MainImHeight, BpPix, 3);
        glGenTextures(1, MainTxtName);
        glBindTexture (GL_TEXTURE_2D, *MainTxtName);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, 3, *MainImWidth, *MainImHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, TextDataPtr);
        InitDrawStIm = 0;
    }

    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, *MainTxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    glColor3ub(0, 0, 0);
    glBegin(GL_QUADS);
        glTexCoord2d(0,0); glVertex2i(0, 0);
        glTexCoord2d(0,1); glVertex2i(0, m * 10);
        glTexCoord2d(1,1); glVertex2i(n * 10, m * 10);
        glTexCoord2d(1,0); glVertex2i(n * 10, 0);
    glEnd();
}

void draw_style_image(int *MainImWidth, int *MainImHeight, GLuint *MainTxtName, stbi_uc *TextDataPtr, int *BpPix)
{
    if (initStyle)
    {
        if (TextDataPtr != NULL)
            stbi_image_free(TextDataPtr);
        glDeleteTextures(1, MainTxtName);
        char strDigital [3][50] = {"texture/digital/texdg1.jpg", "texture/digital/texdg2.jpg", "texture/digital/texdg3.jpg"};
        char strNature  [3][50] = {"texture/nature/texnt1.jpg", "texture/nature/texnt2.jpg", "texture/nature/texnt3.jpg"};
        char strSnow    [3][50] = {"texture/snow/texsn1.jpg", "texture/snow/texsn2.jpg", "texture/snow/texsn3.jpg"};

        int a = rand()%3;
        char buff[50];
        switch (OrderStyle)
        {

        case DigitalTexM:
            strcpy(buff, strDigital[a]);
            break;
        case NatureTexM:
            strcpy(buff, strNature[a]);
            break;
        case SnowTexM:
            strcpy(buff, strSnow[a]);
            break;
        }

        glGenTextures(1, MainTxtName);
        TextDataPtr = stbi_load(&buff[0], MainImWidth, MainImHeight, BpPix, 3);
        glGenTextures(1, MainTxtName);
        glBindTexture (GL_TEXTURE_2D, *MainTxtName);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, 3, *MainImWidth, *MainImHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, TextDataPtr);

        initStyle = 0;
    }

    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, *MainTxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    glColor3ub(0, 0, 0);
    glBegin(GL_QUADS);
       glTexCoord2d(0,0); glVertex2i(0, 0);
       glTexCoord2d(0,1); glVertex2i(0, m * 10);
       glTexCoord2d(1,1); glVertex2i(n * 10, m * 10);
       glTexCoord2d(1,0); glVertex2i(n * 10, 0);
    glEnd();
}

void draw_info_image(int *MainImWidth, int *MainImHeight, GLuint *MainTxtName, stbi_uc *TextDataPtr, int *BpPix)
{
    static int InitDrawInfoIm = 1;
    if (InitDrawInfoIm)
    {
        glGenTextures(1, MainTxtName);
        TextDataPtr = stbi_load("Texture/info/info.jpg", MainImWidth, MainImHeight, BpPix, 3);
        glGenTextures(1, MainTxtName);
        glBindTexture (GL_TEXTURE_2D, *MainTxtName);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, 3, *MainImWidth, *MainImHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, TextDataPtr);
        InitDrawInfoIm = 0;
    }

    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, *MainTxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    glColor3ub(0, 0, 0);
    glBegin(GL_QUADS);
        glTexCoord2d(0,0); glVertex2i(0, 0);
        glTexCoord2d(0,1); glVertex2i(0, m * 10);
        glTexCoord2d(1,1); glVertex2i(n * 10, m * 10);
        glTexCoord2d(1,0); glVertex2i(n * 10, 0);
    glEnd();
}

void move_character(int *x_s, int *y_s, int *x_f, int *y_f, int **maze)
{
    if (!cheatTest)
        {
            if (!finishTest && !waveTest)
            {
                 switch (dir)
                 {
                     case up:    if (maze[*y_s - 1][*x_s] >= -4)
                                 {
                                     if ((*y_s - 1 == *y_f) && (*x_s == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *y_s -= 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                     case right: if (maze[*y_s][*x_s + 1] >= -4)
                                 {
                                     if ((*y_s == *y_f) && (*x_s + 1 == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *x_s += 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                     case down:  if (maze[*y_s + 1][*x_s] >= -4)
                                 {
                                     if ((*y_s + 1 == *y_f) && (*x_s == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *y_s += 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                     case left:  if (maze[*y_s][*x_s - 1] >= -4)
                                 {
                                     if ((*y_s == *y_f) && (*x_s - 1 == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *x_s -= 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                 }
            }
        }
        else
        {
            if (!finishTest && !waveTest)
            {
                 switch (dir)
                 {
                     case up:    if (maze[*y_s - 1][*x_s] >= -10)
                                 {
                                     if ((*y_s - 1 == *y_f) && (*x_s == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *y_s -= 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                     case right: if (maze[*y_s][*x_s + 1] >= -10)
                                 {
                                     if ((*y_s == *y_f) && (*x_s + 1 == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *x_s += 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                     case down:  if (maze[*y_s + 1][*x_s] >= -10)
                                 {
                                     if ((*y_s + 1 == *y_f) && (*x_s == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *y_s += 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                     case left:  if (maze[*y_s][*x_s - 1] >= -10)
                                 {
                                     if ((*y_s == *y_f) && (*x_s - 1 == *x_f))
                                         finishTest++;
                                     maze[*y_s][*x_s] = 1;
                                     *x_s -= 1;
                                     maze[*y_s][*x_s] = 2;
                                     moveTest = 0;
                                 }
                         break;
                 }
            }
        }
}

void draw_wall(int *x_s, int *y_s, int *x_f, int *y_f, GLuint *MainTxtName, int m, int n, int **maze)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, *MainTxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
        {
            if (maze[i][j] >= -4)
            {
                glColor3ub(0, 0, 0);
                glBegin(GL_QUADS);
                    glTexCoord2d(0,0); glVertex2i(0 + j*10, 0 + i*10);
                    glTexCoord2d(0,0); glVertex2i(10 + j*10, 0 + i*10);
                    glTexCoord2d(0,0); glVertex2i(10 + j*10, 10 + i*10);
                    glTexCoord2d(0,0); glVertex2i(0 + j*10, 10 + i*10);
                glEnd();
            }

            if (maze[i][j] == -20)
            {
                glColor3ub(47, 79, 79);
                glBegin(GL_QUADS);
                    glVertex2i(0 + j*10, 0 + i*10);
                    glVertex2i(10 + j*10, 0 + i*10);
                    glVertex2i(10 + j*10, 10 + i*10);
                    glVertex2i(0 + j*10, 10 + i*10);
                glEnd();
            }

            if (maze[i][j] == -4)
            {
                glColor3ub(245, 245, 220);
                glPointSize(3);
                glBegin(GL_POINTS);
                   glVertex2i(5 + j*10, 5 + i*10);
                glEnd();
            }

            if (j == *x_f && i == *y_f)
            {
                glColor3ub(255, 0, 0);
                glBegin(GL_QUADS);
                    glVertex2i(0 + j*10 + 2, 0 + i*10 + 2);
                    glVertex2i(10 + j*10 - 2, 0 + i*10 + 2);
                    glVertex2i(10 + j*10 - 2, 10 + i*10 - 2);
                    glVertex2i(0 + j*10 + 2, 10 + i*10 - 2);
                glEnd();

                glColor3ub(255, 20, 147);
                glBegin(GL_TRIANGLES);
                    glVertex2i(0 + j*10 + 5, 0 + i*10 + 5);
                    glVertex2i(0 + j*10 - 2, 0 + i*10 - 10);
                    glVertex2i(10 + j*10 + 2, 0 + i*10 - 10);
                glEnd();
            }

            if (j == *x_s && i == *y_s)
            {
                if (finishTest > 3)
                {
                    glColor3ub(255, 0, 0);
                    glBegin(GL_QUADS);
                        glVertex2i(0 + j*10 + 2, 0 + i*10 + 2);
                        glVertex2i(10 + j*10 - 2, 0 + i*10 + 2);
                        glVertex2i(10 + j*10 - 2, 10 + i*10 - 2);
                        glVertex2i(0 + j*10 + 2, 10 + i*10 - 2);
                    glEnd();

                    glColor3ub(255, 20, 147);
                    glBegin(GL_TRIANGLES);
                        glVertex2i(0 + j*10 + 5, 0 + i*10 + 5);
                        glVertex2i(0 + j*10 - 2, 0 + i*10 - 10);
                        glVertex2i(10 + j*10 + 2, 0 + i*10 - 10);
                    glEnd();
                    finishTest++;
                    if (finishTest == 6)
                    {
                        finishTest = 1;
                        finishTestSw++;
                    }
                }
            else
                {
                    glColor3ub(0, 255, 0);
                    glBegin(GL_QUADS);
                        glVertex2i(0 + j*10 + 2, 0 + i*10 + 2);
                        glVertex2i(10 + j*10 - 2, 0 + i*10 + 2);
                        glVertex2i(10 + j*10 - 2, 10 + i*10 - 2);
                        glVertex2i(0 + j*10 + 2, 10 + i*10 - 2);
                    glEnd();

                    glColor3ub(173, 255, 47);
                    glBegin(GL_TRIANGLES);
                        glVertex2i(0 + j*10 + 5, 0 + i*10 + 5);
                        glVertex2i(0 + j*10 - 2, 0 + i*10 - 10);
                        glVertex2i(10 + j*10 + 2, 0 + i*10 - 10);
                    glEnd();
                    if (finishTest > 0)
                    {
                        finishTest++;
                        finishTestSw++;
                        winTest=1;
                    }
                }
            }

            if (   waveTest
                && maze[i][j] > 1
                && !(j == *x_f && i == *y_f)
                && !(j == *x_s && i == *y_s))
            {
                glColor3ub(0, 0, 255);
                glBegin(GL_QUADS);
                    glVertex2i(0 + j*10, 0 + i*10);
                    glVertex2i(10 + j*10, 0 + i*10);
                    glVertex2i(10 + j*10, 10 + i*10);
                    glVertex2i(0 + j*10, 10 + i*10);
                glEnd();
            }
        }
}

void draw_finish_image(int *MainImWidth, int *MainImHeight, GLuint *MainTxtName, stbi_uc *TextDataPtr, int *BpPix)
{
    static int InitDrawFinIm = 1;
    if (InitDrawFinIm)
    {
        glGenTextures(1, MainTxtName);
        TextDataPtr = stbi_load("Texture/Finish/texture1.jpg", MainImWidth, MainImHeight, BpPix, 3);
        glGenTextures(1, MainTxtName);
        glBindTexture (GL_TEXTURE_2D, *MainTxtName);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, 3, *MainImWidth, *MainImHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, TextDataPtr);
        InitDrawFinIm = 0;
    }

    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, *MainTxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    glColor3ub(0, 0, 0);
    glBegin(GL_QUADS);
       glTexCoord2d(0,0); glVertex2i(0, 0);
       glTexCoord2d(0,1); glVertex2i(0, SCREEN_HEIGHT);
       glTexCoord2d(1,1); glVertex2i(SCREEN_WIDTH, SCREEN_HEIGHT);
       glTexCoord2d(1,0); glVertex2i(SCREEN_WIDTH, 0);
    glEnd();
}
