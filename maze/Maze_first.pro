TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_LFLAGS += -static-libgcc

QMAKE_CFLAGS += -std=c11
        QT += opengl
        LIBS += -lopengl32

SOURCES += main.c \
    mazecreation.c \
    mazeoutput.c \
    solvingbyfullexpl.c \
    solvingbywave.c \
    menu.c

win32: LIBS += -L$$PWD/ -lglfw3
win32: LIBS += -L$$PWD/ -lglfw3dll
win32: LIBS += -L$$PWD/ -lglut32
win32: LIBS += -L$$PWD/ -lglaux
win32: LIBS += -L$$PWD/ -lgdi32


HEADERS += \
    mazecreation.h \
    mazeoutput.h \
    solvingbyfullexpl.h \
    solvingbywave.h \
    glfw3.h \
    glfw3native.h \
    glut.h \
    glaux.h \
    stb_image.h \
    stb.h \
    menu.h
