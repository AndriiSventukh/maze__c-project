# Maze (C-project)  

Simple game on C, Using OpenGl  

At first, using quite simple initial menu player can select desirable level and theme and start game.  
For this purpose only keyboard is needed.  
![Image](game_screens/game_field.png)  

Info is available by pressing "ESC" button. It describes rules and necessary buttons.  
![Image](game_screens/info_menu.png)  

While playing, game menu is available:  
![Image](game_screens/play_menu.png)  

Player can also ask for prompt:  
![Image](game_screens/prompt.png)  

After player got bored or gave up, he or she can launch "wave" (the wave algorithm):  
![Image](game_screens/wave.png)  

That algorithm will find and show the right way:  
![Image](game_screens/right_way.png)  

In any case after either player found out the way on his/her own or the "wave" helped, the hidden background become visible:  
![Image](game_screens/win.png)  
